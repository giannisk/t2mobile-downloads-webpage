# T2Mobile Downloads Page - Webpage Design

This webpage was created about three years ago. It does no longer follow latest design trends.

![Preview](/img/preview.png)

Based on the [Free Flat UI KIT](https://www.behance.net/gallery/22797537/Free-Flat-UI-KIT).

## License

Final work is made available under the [MIT License](LICENSE).

Free Flat UI KIT is available under [Creative Commons Attribution 3.0 Unported](https://creativecommons.org/licenses/by/3.0/legalcode).

## Disclaimer

This webpage is an individual effort. It is not affiliated with T2Mobile in any way.

All trademarks, product names and logos appearing are the property of their respective owners.
